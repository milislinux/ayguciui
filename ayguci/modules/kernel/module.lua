string.split=util.string.split
string.trim=util.string.trim
shell=util.shell
check_command=util.check_command



function get_kmodule_info(kmod)
	local cmd="systool -vm %s"
	local info={}
	info.info={}
	info.attributes={}
	info.params={}
	info.sections={}
	local mode=""
	for line in shell(cmd:format(kmod)):gmatch("[^\r\n]+") do
      if line:match("Attributes") then
		mode="attributes"
      elseif line:match("Parameters") then
		mode="params"
	  elseif line:match("Sections") then
		mode="sections"
	  elseif line:match("Module") then
		mode="info"
	  end
	  local key=line:split("=")[1]
	  local val=line:split("=")[2]
	  if key ~= nil and val ~= nil then
		  print(mode,key,val)
		  key=key:trim()
		  val=val:trim()
		  info[mode][key] =val
	  end
	end
	--print(serpent.block(info))  
	return info
end

function run()
	ret.data={}
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			if req.modul == nil then 
				ret.data={error="module field is empty"}
			else
				ret.data[req.modul]=get_kmodule_info(req.modul)
			end
			if next(ret.data) == nil then ret={error="undefined get query"} end
		end
	else
		ret.api={
			title="Kernel Modül Bilgisi",
			auth=false,
			method={
				get={
					modul="bilgi alınacak modul",
				},
				--post={
				--},
			},
		}
	end
	response=json.encode(ret)
end

