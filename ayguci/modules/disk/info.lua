string.split=util.string.split

local function get_info(disk)
	local infos={}
	local info={}
	local k,v=""
	local komut="lsblk -P -no TYPE,NAME,LABEL,PATH,MOUNTPOINT,FSSIZE,FSTYPE,FSUSED,FSUSE%,SIZE,UUID"
	for line in util.shell(komut):gmatch("[^\r\n]+") do 
		for val in line:gmatch("%S+") do
			k=val:split("=")[1]
			v=val:split("=")[2]
			info[k:lower()]=v:gsub('"',"")
		end
		infos[info.name]=info
		--table.insert(infos,info) 
		info={}
	end
	if disk then
		return infos[disk]
	else 
		return infos
	end
end

function run()
	ret.data={}
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			if req.disk == nil then 
				ret.data=get_info()
			else
				ret.data=get_info(req.disk)
			end
			if next(ret.data) == nil then ret={error="undefined get query"} end
		end
	else
		ret.api={
			title="Disk Bilgisi",
			auth=false,
			method={
				get={
					disk="bilgi alınacak disk",
				},
			},
		}
	end
	response=json.encode(ret)
end
