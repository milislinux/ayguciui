shell=util.shell

function get_utc()
	local get_cmd="servis oku clock.utc"
	return shell(get_cmd)
end

function run()
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			ret.data=get_utc()
		end
		if req.method == "post" then
			if req.utc_status then
				local set_cmd='servis yaz clock utc %s'
				shell(set_cmd:format(req.utc_status))
				ret.status="OK"
			end
			ret.data=get_utc()
		end
	else
		ret.api={
			info="Tarih/Saat UTC Bilgisi",
			auth=false,
			method={
				get={},
				post={
					utc_status="utc_bilgisi",
				}
			},
		}
	end
	response=json.encode(ret)
end
