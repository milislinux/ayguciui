shell=util.shell

function get_timezone()
	local get_cmd="servis oku clock.timezone"
	return shell(get_cmd)
end

function run()
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			ret.data=get_timezone()
		end
		if req.method == "post" then
			if req.zone then
				local set_cmd='servis yaz clock timezone %s'
				shell(set_cmd:format(req.zone))
				-- clock servisi çalıştırmak gerekir
				shell("servis kos clock")
				ret.status="OK"
			end
			ret.data=get_timezone()
		end
	else
		ret.api={
			info="Tarih/Saat Saat Dilimi",
			auth=false,
			method={
				get={},
				post={
					zone="timezone_bilgisi",
				}
			},
		}
	end
	response=json.encode(ret)
end
