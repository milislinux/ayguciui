shell=util.shell
check_command=util.check_command

function get_datetime()
	local get_cmd="date +'%m/%d/%Y %T'"
	return shell(get_cmd)
end

function run()
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			ret.data=get_datetime()
		end
		if req.method == "post" then
			if req.value then
				local set_cmd='hwclock --set --date "%s" && hwclock -s'
				shell(set_cmd:format(req.value))
			end
		end
	else
		ret.api={
			info="Tarih/Saat Elle Ayarlama",
			auth=false,
			method={
				get={},
				post={
					value="format=%m/%d/%Y %h:%m:%s",
				}
			},
		}
	end
	response=json.encode(ret)
end
