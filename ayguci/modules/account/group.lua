local shell = util.shell
local get_content = util.get_content

local file="/etc/group"
local groups={}
local users={}

for line in get_content(file):gmatch("[^\r\n]+") do
	--group=line:match("([%w]+):")
	local name,x,id,_users = line:match("(['%w%d-_']+):(['%w']+):(['%d']+):(.*)")
	--if _users ~= nil then
		--for user in _users:gmatch("[^,\n]+") do table.insert(users,user) end
	--end
	groups[name]={id=id,users=_users}
	--users={}
end

function run()
	ret.data={}
	
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			--users,input,disk,network,netdev,floppy,fuse,video,lp,tty,audio,cdrom,scanner,adm,vboxusers,wheel,pulse-access
			local default_groups={"tty","floppy","disk","lp","audio","video","cdrom","adm","network","input","wheel","users"}
			if req.query == nil then req.query = "all"  end
			if req.query == "all" then
				ret.data=groups
			end
	
			if req.query == "default" then
				ret.data={default_groups=default_groups}
			end
			if req.query == "info" then
				if req.user ~= nil then
					ret.data[req.user]={groups={}}
					for nm,gr in pairs(groups) do	
						if gr.users:find(req.user) or gr.users:find(","..req.user) or nm == req.user then
							table.insert(ret.data[req.user]["groups"],nm)
						end
					end
				elseif req.group ~= nil then
					ret.data[req.group]={}
					if tonumber(groups[req.group].id) > 999 then
						groups[req.group].users=req.group
					end
					ret.data[req.group]=groups[req.group]
				else
					ret.data={error="user or group parameter not exist"}
				end 
			end
			if next(ret.data) == nil then ret={error="undefined get query"} end			
		end
		
		if req.method == "post" then
			
			local group=""
			
			if req.oper == "del"  then
				local force=" -f "
				local cmd="groupdel "
				if req.group ~= nil then
					group=req.group
					if req.force and req.force == false then
						force=""
					end
					if groups[group] ~= nil then
						cmd=cmd..force..group
						ret.data.cmd=cmd
					else
						ret.error=group.." group is not exist"
					end
				else
					ret.error="group field is mandatory"
				end
			end
			
			if req.oper == "add"  then
				local id=""
				local cmd="groupadd "
				if req.group ~= nil then
					
					if req.id ~= nil then
						id=(" -g %s "):format(req.id)
					end
					group=req.group
					cmd=cmd..id..group
					--check if already exist
					if groups[group] == nil then
						ret.data.cmd=cmd
					else
						ret.error=group.." group is already exist"
					end
					--shell(cmd)
				else
					ret.error="group field is mandatory"
				end
			end
		end
		
	else
		ret.api={
			title="Grup Yönetimi",
			auth=false,
			method={
				get={
					query="bilgi_tipi:*all*/default/info",
					user="kullanıcının grupları:query=info için parametre",
					group="grup kullanıcıları:query=info için parametre",
				},
				post={
					oper={
						add={
							group="group name, mandatory",
							id="group id, optional",
						},
						del={
							group="group name, mandatory",
							force="force remove,default:true, optional",
						},
					},
				},
			},
		}
	end
	response=json.encode(ret)
end

