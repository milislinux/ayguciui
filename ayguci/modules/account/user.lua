local shell = util.shell
local get_content = util.get_content

function get_user_list()
	local file="/etc/passwd"
	local users={}
	for line in get_content(file):gmatch("[^\r\n]+") do
		local name,x,id,gid,desc,home,shell=line:match("(['%w%d-_']+):(['%w']+):(['%d']+):(['%d']+):(.*):(['%w%d-_/']+):(['%w/']+)")
		if name == nil then   
			name,x,id,gid,desc,home,shell=line:match("(['%w%d-_']+):(['%w']+):(['%d']+):(['%d']+)(['::']+)(['%w%d-_/']+):(['%w/']+)")	
			desc=""
		end
		users[name]={id=id,gid=gid,desc=desc,home=home,shell=shell}
	end
	return users
end

function run()
	ret.data={}
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			local sys_users={}
			local std_users={}
			for name,_user in pairs(get_user_list()) do
				if tonumber(_user.id) < 1000 then
					sys_users[name]=_user
				else
					std_users[name]=_user
				end
			end
			if req.query == nil then req.query = "all"  end
			if req.query == "all" then
				ret.data={users=get_user_list()}
			end
			if req.query == "system" then
				ret.data={sys_users=sys_users}
			end
			if req.query == "normal" then
				ret.data={normal_users=std_users}
			end
			if req.query == "info" then
				if req.user ~= nil then
					ret.data={user=get_user_list()[req.user]}
				else
					ret.data={error="user parameter not exist"}
				end 
			end
			if next(ret.data) == nil then ret={error="undefined get query"} end
		end
		
		if req.method == "post" then
			
			local user=""
			
			if req.oper == "del"  then
				local force=" -f -r "
				local cmd="userdel "
				if req.user ~= nil then
					user=req.user
					if req.force == false then
						force=""
					end
					if get_user_list()[user] ~= nil then
						cmd=cmd..force..user
						shell(cmd)
						ret.data=cmd
					else
						ret.error=user.." user is not exist"
					end
				else
					ret.error="user field is mandatory"
				end
			end
			
			if req.oper == "add"  then
				local homedir=" -M "
				local desc=""
				local id=""
				local groups=""
				local sh=""
				local cmd={add="useradd ",edit="usermod "}
				local mode=""

				if req.user ~= nil then
					-- mod tespiti için
					-- kullanıcı var ise edit yoksa add
					if get_user_list()[req.user] == nil then
						mode="add"
					else
						mode="edit"
					end
					if req.home ~= nil then
						if req.home == "true" then
							homedir=(" -m -d /home/%s "):format(req.user)
						--else 
						--	homedir=(" -m -d %s "):format(req.home)
						end
					end
					if req.desc ~= nil then
						desc=(" -c %s "):format(req.desc)
					else
						desc=(" -c %s "):format(req.user)
					end
					if req.shell ~= nil then
						sh=(" -s %s "):format(req.shell)
					else
						sh=(" -s /bin/bash "):format(req.shell)
					end
					if req.groups and next(req.groups) ~= nil then
						for _,gr in ipairs(req.groups) do
							groups=groups..gr..","
						end
						groups="-G '"..groups:sub(1,-2).."' "
					end
					if req.id ~= nil then
						id=(" -u %s "):format(req.id)
					end
					user=req.user
					local _cmd=""
					if mode == "add" then
						_cmd=cmd.add..homedir..groups..id..desc..sh..user
					else
						_cmd=cmd.edit..groups..desc..user
					end	
					shell(_cmd)
					ret.data=_cmd
				else
					ret.error="user field is mandatory"
				end
			end
			
			if req.oper == "chpass"  then
				local cmd="echo '%s:%s' | chpasswd -e"
				local enc_cmd="cat /etc/shadow | grep %s | cut -d: -f2"
				--local cmp_cmd="echo '%s' | openssl passwd -6 -salt %s -stdin"
				--local old_cmd="cat /etc/shadow | grep %s | cut -d: -f2"
				if req.user and req.newpasswd then
					user=req.user
					local new=req.newpasswd
					if get_user_list()[user] ~= nil then
						shell(cmd:format(user,new))
						ret.data="user password changed"
					else
						ret.error="user is not defined"
					end
				else
					ret.error="user, newpasswd field is mandatory"
				end
			end
			
		end
	else
		ret.api={
			title="Kullanıcı Yönetimi",
			auth=false,
			method={
				get={
					query="bilgi_tipi:*all*/system/normal/info",
					user="kullanıcı:query=info için parametre",
				},
				post={
					oper={
						add={
							user="user name, mandatory",
							home="default:no ,true or valid directory",
							desc="user definiton",
							id="user id , optional",
							shell="user shell, optional, default:/bin/bash",
							groups="groups seperated with comma",
						},
						del={
							user="user name, mandatory",
							force="force remove,default:true, optional",
						},
						chpass={
							user="user name, mandatory",
							newpasswd="sha256 hashed new password, mandatory",
						},
					},
				},
			},
		}
	end
	response=json.encode(ret)
end
