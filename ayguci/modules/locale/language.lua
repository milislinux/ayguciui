shell=util.shell

function default_locales()
	local cmd="locale -a"
	local _locales={}
	for line in shell(cmd):gmatch("[^\r\n]+") do
		table.insert(_locales,line)
	end
	return {locales=_locales}
end

function sys_lang(lang)
	local cmd="servis yaz locale language"
	local cmdr="servis oku locale.language"
	if lang ~= nil then
		local valid=false
		for _,l in ipairs(default_locales().locales) do
			if l == lang then 
				valid=true 
				break
			end
		end
		if valid then shell(cmd..lang)
		else 
			return {error="language is not valid"}
		end
	end
	return {language={system=shell(cmdr)}}
end

function keyb_lang(lang)
	local cmd="servis yaz system keyboard"
	local cmdr="servis oku system.keyboard"
	if lang ~= nil then
		-- check if it is valid
		shell(cmd..lang)
	end
	return {language={keyboard=shell(cmdr)}}
end

function run()
	ret.data={}
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			if req.query == nil then req.query = "system"  end
			if req.query == "keyboard" then
				ret.data=keyb_lang()
			end
			if req.query == "system" then
				ret.data=sys_lang()
			end
			if req.query == "locales" then
				ret.data=default_locales()
			end
		end
		if req.method == "post" then
			if req.system then
				ret.data=sys_lang(req.system)
			end
			if req.keyboard then
				ret.data=keyb_lang(req.keyboard)
			end
		end
	else
		ret.api={
			info="Yerelliştirme Dil Yönetimi",
			auth=false,
			method={
				get={
					query="locales",
				},
				post={
					system="sistem dili",
					keyboard="klavye dili",
				}
			},
		}
	end
	response=json.encode(ret)
end
