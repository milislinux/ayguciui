local function get_info()
	ret.data=json.decode(util.shell("rfkill -J"))
	--print("--------------------",ret.data)
end

function run()
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			get_info()
		end
	else
		ret.api={
			info="Kablosuz Cihazların Bloke Durumu",
			auth=false,
			method={get={}},
		}
	end
	response=json.encode(ret)
end
