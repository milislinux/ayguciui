local get_content = util.get_content
string.split=util.string.split
string.trim=util.string.trim
shell=util.shell

-- one line content - fist line also
local function content(file)
	for _name in get_content(file):gmatch("[^\r\n]+") do		
		if _name == "FileNotFound" then return "-" end
		return _name:gsub("%s+", "")
	end
	return "-"
end

local function get_cpu_info()
	local file="/proc/cpuinfo"
    local cpu={}
	for name in get_content(file):gmatch("[^\r\n]+") do
		if name:split(":")[1]:gsub("%s+", "") == "modelname" then
			cpu.model=name:split(":")[2]
		end
		if name:split(":")[1]:gsub("%s+", "") == "cpucores" then
			cpu.core=name:split(":")[2]
		end
	end
	ret.data.cpu=cpu
end

local function get_ram_info()
	--ret.data.ram={info="not ready"}
	ret.data.ram=content ("/proc/meminfo"):split(":")[2]
end

local function get_video_info()
	ret.data.video={
		VGA=shell("lspci | grep VGA"),
		--["3D"]=shell('GPU=$(lspci | grep 3D | cut -d ":" -f3);RAM=$(cardid=$(lspci | grep 3D |cut -d " " -f1);lspci -v -s $cardid | grep " prefetchable"| cut -d "=" -f2);echo $GPU $RAM')
	}
end

local function get_os_info()
	ret.data.os={
		linux=shell("lsb_release -i"):split(":")[2]:trim(),
		hostname=shell("hostname"),
		kernel=shell("uname -r"),
		version=shell("lsb_release -r"):split(":")[2]:trim(),
		codename=shell("uname -r"):split(":")[2],
		release=content "/etc/milis-surum",
		uptime=shell("uptime"),
	}
end

local function get_info()
	ret.data={
		uuid   = content "/sys/class/dmi/id/product_uuid",
		vendor = content "/sys/class/dmi/id/board_vendor",
		board = content "/sys/class/dmi/id/board_name",
		product = content "/sys/class/dmi/id/product_name",
		serial = content "/sys/class/dmi/id/product_serial",
		bios={
			vendor = content "/sys/class/dmi/id/bios_vendor",
			version = content "/sys/class/dmi/id/bios_version",
			release = content "/sys/class/dmi/id/bios_release",
			date = content "/sys/class/dmi/id/bios_date",
		},
	}
end

function run()
	ret.data={}
	if request and  request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			if req.query == nil then req.query = "all"  end
			if req.query == "sum" or req.query == "all" then get_info() end
			if req.query == "os" or req.query == "all" then get_os_info() end
			if req.query == "cpu" or req.query == "all" then get_cpu_info() end
			if req.query == "ram" or req.query == "all" then get_ram_info() end
			if req.query == "video" or req.query == "all" then get_video_info() end
			if next(ret.data) == nil then ret={error="undefined get query"} end
		end
		if req.method == "post" then
			local cmd="servis yaz hostname data"
			local cmdr="servis oku hostname.data"
			if req.hostname then
				local set_cmd=cmd.." %s"
				shell(set_cmd:format(req.hostname))
				shell("service start hostname")
				ret.status="OK"
			end
			ret.data=shell(cmdr)
		end
	else
		ret.api={
			title="Sistem Bilgileri (CPU, RAM vs.)",
			auth=false,
			method={
				get={
					query="bilgi_tipi:sum/cpu/ram/*all*",
				},
				post={
					hostname="bilgisayar_adı bilgisi",
				},
			},
		}
	end
	response=json.encode(ret)
end
