shell=util.shell

function get_brightness()
	local int_cmd="ls /sys/class/backlight/"
	local interface=shell(int_cmd)
	local max_brg=shell("cat /sys/class/backlight/"..interface.."/max_brightness")
	local cmd="cat /sys/class/backlight/%s/brightness"
	local val=shell(cmd:format(interface))
	local valp = math.floor(tonumber(val) / tonumber(max_brg) * 100)
	return interface,max_brg,valp
end

function get_info()
	return {[interface]={max=max_brg,value=val,percent=valp}}
end

function set(input)
	interface,max_brg,valp=get_brightness()
	if input.data then
		--print("gelen data:",data.brightness)
		val=tonumber(input)
		if val>99 then val=99 end
		val = val /100 * tonumber(max_brg)
		val = math.floor(val + 0.5 - (val + 0.5) % 1)+1
		local set_cmd="echo %s > /sys/class/backlight/%s/brightness"
		shell(set_cmd:format(val,interface))
	end
end

function run()
	if request and request.data then
		req=json.decode(request.data)
		if req.method == "get" then
			ret.data=get_brightness()
		end
	else
		ret.api={
			info="Ekran parlaklık işlevi",
			auth=false,
			method={
				get={
				},
			},
		}
	end
	response=json.encode(ret)
end
