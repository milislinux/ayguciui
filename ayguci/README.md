## Ayguci

Milis Linux Sistemi Bilgi ve Ayar Uygulaması


```
# Aşağıdaki komutları yetkili olarak uygulayın.
# Kurulum

# Ayguci grafiksel arayüz kurma.
mpsc -b ayguci @ayguci
mps gun -B

# Ayguci kabuk arayüz başlatma.
ayguci shell

# Ayguci grafiksel arayüz kurma.
mpsc -b ayguciui @ayguciui
mps gun -B
```
