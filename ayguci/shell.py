#!/bin/python3
import readline
import logging
import sys
import json
import subprocess

def run(arg):
	s_dir = "/usr/milis/ayguciui/ayguci/cli.lua"
	get = subprocess.Popen([s_dir,arg], stdout = subprocess.PIPE)
	get.wait()
	get = get.communicate()[0]
	get = get.decode("utf-8","strict")
	return get

# = i tanımama durumunda delims grubundan siliyoruz.
old_delims = readline.get_completer_delims()
readline.set_completer_delims(old_delims.replace('=', ''))

def call(link,param):

	# Send data
	message = link
	print ('l->"%s"' % message)
	print ('p->"%s"' % param)
	# parse params
	_msg=""
	prms={}
	temp_p1=""
	for par in param:
		if "=" in par:
			p1=par.split("=")[0]
			p2=par.split("=")[1]
			prms[p1]=p2
			#temp_p1=p1
		else:
			prms[p1]="'"+prms[p1]+" "+par+"'"
	for key in prms:
		_msg+=f'"{key}" : "{prms[key]}",'
		#_msg+=f'"{p1}" : "{p2}",'
	if param:
		_msg=_msg[0:-1]
		message+="^"+"{"+_msg+"}"

	data=run(message)

	#print("data->",data.decode())
	json_object = json.loads(data)
	# json dumps ettikten sonra \u şeklinde yazmaması için ensure_ascii=False
	response = json.dumps(json_object, indent=2, ensure_ascii=False)
	print(response)


funcs={
	"system":lambda x:call("system/info",x),
	"rfkill":lambda x:call("system/rfkill",x),
	"user":lambda x:call("account/user",x),
	"group":lambda x:call("account/group",x),
	"datetime":lambda x:call("datetime/manual",x),
	"utc":lambda x:call("datetime/utc",x),
	"timezone":lambda x:call("datetime/timezone",x),
	"kmod":lambda x:call("kernel/module_info",x),
	"lang":lambda x:call("locale/language",x),
	"kmod":lambda x:call("kernel/module",x),
	"disk":lambda x:call("disk/info",x),
}

# https://pymotw.com/3/readline/index.html#completing-text
class SimpleCompleter(object):
    
    def __init__(self, options):
        self.options = sorted(options)
        return

    def complete(self, text, state):
        response = None
        if state == 0:
            # This is the first time for this text, so build a match list.
            if text:
                self.matches = [s 
                                for s in self.options
                                if s and s.startswith(text)]
                #logging.debug('%s matches: %s', repr(text), self.matches)
            else:
                self.matches = self.options[:]
                #logging.debug('(empty input) matches: %s', self.matches)
        
        # Return the state'th item from the match list,
        # if we have that many.
        try:
            response = self.matches[state]
        except IndexError:
            response = None
        #logging.debug('complete(%s, %s) => %s', repr(text), state, repr(response))
        return response
    
def shell():
    line = ''
    while line != 'stop':
        line = input('ayguci# ')
        fun_key=line.split()[0]
        fun_par=line.split()[1:]
        funcs[fun_key](fun_par) if fun_key in funcs else print ('bulunamadı! %s' % line)

# Register our completer function
readline.set_completer(SimpleCompleter([
	"method=get",
	"method=post",
	"query=info",
	"query="
]+list(funcs.keys())).complete)

# oto tamamlama özelliği
readline.parse_and_bind('tab: complete')

# ayguci shell girdi döngüsü
shell()
