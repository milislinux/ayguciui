#!/usr/bin/slua

-- eski depoyu sil

local ayguci_dir = "/usr/milis/ayguciui/ayguci"

package.path  = ayguci_dir.."/kod/?.lua"   .. ";".. package.path

l5=require("l5")
json=require("json")
serpent=require("serpent")
util=require("util")

string.split=util.string.split

stat=function(file) return l5.lstat3(file) end

request={}
ret={}

local modules_dir=ayguci_dir.."/modules/"
local delim="%^"

local argument=arg[1]

if request then
	local modul=argument:split(delim)[1]
	local param=argument:split(delim)[2]
	if modul then
		local mdizin=modul:split("/")[1]
		local mfun=modul:split("/")[2]
		if stat(modules_dir..modul..".lua") then
			package.path  = modules_dir.. mdizin.."/?.lua"   .. ";".. package.path
			local md=require(mfun)
			request.data=param
			run()
			print(response)
		else
			print("modul yok",modul)
		end
	end
end
