### Bu proje rafa kaldırılmıştır. Güncel Hali: [Ayguci](https://gitlab.com/milislinux/ayguci)

# AyguciUI

Ayguci için arayüz uygulaması.

# Ağ - Kablolu ve Kablosuz
    - Ağ kartı varmı? Kablolu ve veya Kablosuz
    - Bir ağa bağlı mı?
    - Bir ağa bağlanma bağlı ağdan kopma vb.

# Bluetooth
    - Aygıt aktif mi?
    - Aygıtı aktif etme
    - Cihazları listeleme
    - Chazla bağlantı kurma

# Arkaplan
    - Mevcut arkaplan nedir?
    - Arka plan atama

# Bildirimler
    - Bildirimlerin konumu, fontu vb. nedir?
    - Bildirim konumu, fontu vb. atama
    - Kabaca fnott configinden istenilenleri seçerek düzenleme

# Ses
    - Ses çıkış aygıtları neler?
    - Ses çıkış aygıtı atama
    - Ses giriş aygıtları neler?
    - Ses giriş aygıtı atama
    - Ses çıkış ve giriş aygıtlarının seviyeleri neler?
    - Ses çıkış ve giriş ses atama

# Güç
    - Pil varmı?
    - Pil şarj durumu?
    - Ekran parlaklığı aktif mi?
    - Ekran parlaklığı nedir?
    - Ekran parlaklığı atama
    - Ekranı karartma ve süresi
    - Ekranı askıya alma ve süresi

# Ekranlar
    - Sisteme bağlı ekranlar neler?
    - Ekranların çözünürlükleri neler?
    - Hangi ekrandan görüntü veriliyor?
    - Görüntüyü ekrana gönderme
    - Görüntü çözünürlüğü değiştirme
 
 
 # Fare Ve Klavye
     - Fare veya dokunmatik yüzey varmı?
     - Dokunmatik yüzey kaydırma yönü nedir?
     - Dokunmatik yüzey kaydırma yönü atama
     - Dokunbatik yüzey - Fare hızı nedir?
     - Dokunmatik yüzey - Fare hızı atama
     - Kenar kaydırma aktif mi?
     - Kenar kaydırma atama
     - Klavye türü-varyantı nedir?
     - Klavye türü-varyantı atma

# Yazıcılar
    - Sisteme ekli yazıcılar nelerdir?
    - Yazıcı ekleme

# Bölge ve Dil
    - Aktif sistem dili nedir?
    - Aktif sistem dili atama

# Kullanıcılar
    - Sistemdeki kullanıcı nedir parolası nedir?
    - Sisteme kullanıcı ekleme veya değiştirme

# Ön tanımlı uygulamalar
    - Öntanımlı uygulamalar neler web, eposta, takvim, müzik, video vb.
    - Öntanımlı uygulama atama

# Tarih ve saat
    - Saat dilimi nedir?
    - Saat dilimi atama
    - Saati manuel mi ayarlıyor?
    - Manuel saat atama

# Sistem Hakkında
    - Milis Logosu
    - Bellek İşlemci Grafik Disk vs. sistem hakkında bilgi
