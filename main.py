#!/usr/bin/python3
import gi, locale, sys, os, getpass, json
gi.require_version("Gtk", "3.0")
from gi.repository import Gio, Gtk, GLib, GdkPixbuf, Gdk
from agc_lib import (ayguci_parser, pam, translate, system_info, timezone, mimetype,
users, keyboard, wallpaper, theme, screen, document, autostart, panel_config, brightness)

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.translate.keys())
if l not in locales:
	l = "en"
_ = translate.translate[l]


class AyguciUI(Gtk.ApplicationWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.set_title(_[0])

		############################################ BOX AND PANED #############################################
		main_box = Gtk.HBox()
		self.add(main_box)
		paned = Gtk.HPaned()
		main_box.pack_end(paned,True,True,5)
		paned.set_position(200)
		self.set_size_request(1000,550)
		l_box = Gtk.HBox()
		r_box = Gtk.HBox()
		paned.pack1(l_box)
		paned.pack2(r_box)

		##########################################     LEFT          ##########################################
		self.settings_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str)
		self.settings_list = Gtk.TreeView(model=self.settings_store)
		self.settings_list.set_activate_on_single_click(True)
		self.settings_list.connect("row-activated",self.activated_settings_list)
		renderer = Gtk.CellRendererPixbuf()
		coloumn = Gtk.TreeViewColumn(_[1],renderer, gicon = 0)
		self.settings_list.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[2],renderer, text = 1)
		self.settings_list.append_column(coloumn)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.settings_list)
		l_box.pack_start(scroll,True,True,0)

		##########################################     RIGHT          ##########################################
		self.stack = Gtk.Stack()
		self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_UP_DOWN)
		self.stack.set_transition_duration(1000)
		r_box.pack_start(self.stack,True,True,0)
		self.connect("key-press-event",self.key_press)

	def update_stacks(self):
		for stack in self.stack_dict.keys():
			stack = self.stack_dict[stack]
			self.stack.add_titled(stack,stack.name,stack.title)
			self.settings_store.append([stack.icon,stack.name])

	def key_press(self, widget, event):
		key_name = Gdk.keyval_name(event.keyval)
		if key_name == "F6":
			ayguci_parser.create_debug_window()

	def activated_settings_list(self,widget,path,coloumn):
		selection = self.settings_list.get_selection()
		tree_model, tree_iter = selection.get_selected()
		s = tree_model[tree_iter][1]
		self.stack.set_visible_child_name(s)

	def change_stack(self,stack_name):
		items = list(self.stack_dict.keys())
		if stack_name in items:
			self.stack.set_visible_child(self.stack_dict[stack_name])
			self.settings_list.set_cursor(items.index(stack_name))


class Application(Gtk.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="mls.akdeniz.ayguciui", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		GLib.set_application_name("AyguciUI")
		GLib.set_prgname('ayguci ui')
		self.win = None
		self.is_root = "root"
		self.add_main_option("new", ord("n"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "New", None)
		self.add_main_option("user", ord("u"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "User", None)

	def get_root_passwd(self):
		passwd = GetPasswd(None)
		respons = passwd.run()
		if respons == Gtk.ResponseType.DELETE_EVENT:
			sys.exit()
		elif respons == Gtk.ResponseType.OK:
			get_pass = passwd.password_entry.get_text()
			user_name = passwd.user_name_lb.get_text()
			p = pam.pam()
			is_ok = p.authenticate(user_name,get_pass)
			if is_ok:
				passwd.destroy()
				ayguci_parser.user_passwd = get_pass
				return (True,get_pass)
			else:
				passwd.destroy()
				dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _[73])
				dialog.format_secondary_text(_[98])
				dialog.run()
				dialog.destroy()
				return self.get_root_passwd()
		elif respons == Gtk.ResponseType.CANCEL:
			passwd.destroy()
			return (False,False)


	def do_start_up(self):
		Gtk.Application.do_startup(self)

	def do_activate(self):
		if not self.win:
			if self.is_root == "root":
				passwd = self.get_root_passwd()
			else:
				passwd = [False,False]
			self.win = AyguciUI(application=self)
			self.win.stack_dict = {"Theme":theme.Settings(self,_,passwd),
									"MimeTypes":mimetype.Settings(self,_)}
			if passwd[0]:
				if ayguci_parser.test_ayguci():
					self.win.stack_dict = {"System_Info":system_info.Settings(self,_),
									"Timezone":timezone.Settings(self,_,passwd),
									"Users":users.Settings(self,_),
									"Theme":theme.Settings(self,_,passwd),
									"MimeTypes":mimetype.Settings(self,_)}
				else:
					dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _[73])
					dialog.format_secondary_text(_[72])
					dialog.run()
					dialog.destroy()
			json_parse = self.waybar_parse_hjson()

			if os.path.exists(os.path.join(os.path.expanduser("~/.config/masa.ini"))):
				self.win.stack_dict["Keyboard"] = keyboard.Settings(self,_)
				self.win.stack_dict["Wallpaper"] = wallpaper.Settings(self,_)
				self.win.stack_dict["Autostart"] = autostart.Settings(self,_)

			if json_parse:
				self.win.stack_dict["PanelSettings"] = panel_config.Settings(self,json_parse,_)
			else:
				dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _[73])
				dialog.format_secondary_text(_[125])
				dialog.run()
				dialog.destroy()
			if os.path.exists("/usr/bin/wlr-randr"):
				self.win.stack_dict["Screen"] = screen.Settings(self,_)
			else:
				dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _[73])
				dialog.format_secondary_text(_[83])
				dialog.run()
				dialog.destroy()
			md_files = self.parse_md_files()
			if md_files:
				self.win.stack_dict["Document"] = document.Settings(self,_,md_files)
			if os.path.exists("/sys/class/backlight"):
				backlights = os.listdir("/sys/class/backlight")
				if len(backlights) != 0:
					self.win.stack_dict["Brightness"] = brightness.Settings(self,_,backlights)

			self.win.update_stacks()
		self.win.show_all()
		self.win.present()


	def waybar_parse_hjson(self):
		try:
			f = open(os.path.expanduser("~/.config/waybar/config"),"r")
			r_file = f.read()
			f.close()
			js = json.loads(r_file)
			return js
		except:
			return False

	def parse_md_files(self):
		dir_ = "/sources/gitlab.com.milislinux.milis23.git/belge/"
		if os.path.exists(dir_):
			md_files = os.listdir(dir_)
			rt_files = []
			for file_ in md_files:
				f = file_.split(".")
				if f[-1] == "md":
					rt_files.append(os.path.join(dir_,file_))
			if len(rt_files) != 0:
				return rt_files
		return False
				

	def do_command_line(self, command_line):
		options = command_line.get_options_dict()
		options = options.end().unpack()
		if "new" in options:
			if self.win == None:
				self.do_activate()
			ops = options["new"]
			if ops != "":
				self.win.change_stack(ops)
		elif "user" in options:
			ops = options["user"]
			if ops == "normal":
				self.is_root = "normal"
			if ops == "root":
				self.is_root = "root"
		self.activate()
		return False

class GetPasswd(Gtk.Dialog):
	def __init__(self, parent):
		super().__init__(title=_[95],transient_for=parent, flags=0)

		self.set_default_size(300,70)
		self.first_insert = True

		box = Gtk.VBox()

		t_box = Gtk.HBox()
		box.pack_start(t_box,0,0,5)
		normal_user_button = Gtk.Button()
		normal_user_button.set_label(_[126])
		normal_user_button.connect("clicked",self.normal_btn_click)
		t_box.pack_start(normal_user_button,1,1,5)

		t_box = Gtk.HBox()
		box.pack_start(t_box,0,0,5)

		usr_img = Gtk.Image()
		usr_img.set_from_stock(Gtk.STOCK_HOME,Gtk.IconLookupFlags.FORCE_SIZE)
		t_box.pack_start(usr_img,0,0,5)

		self.user_name_lb = Gtk.Label()
		self.user_name_lb.set_text(getpass.getuser())
		t_box.pack_start(self.user_name_lb,0,0,5)

		t_box = Gtk.HBox()
		box.pack_start(t_box,0,0,5)

		key_img = Gtk.Image()
		key_img.set_from_stock(Gtk.STOCK_DIALOG_AUTHENTICATION,Gtk.IconLookupFlags.FORCE_SIZE)
		t_box.pack_start(key_img,0,0,5)

		self.password_entry = Gtk.Entry()
		self.password_entry.set_placeholder_text(_[97])
		self.password_entry.connect("changed",self.password_text_change)
		#self.password_entry.set_visibility(False)
		t_box.pack_start(self.password_entry,1,1,5)

		show_hide_button = Gtk.Button()
		show_hide_button.set_label("*")
		show_hide_button.connect("clicked",self.show_hide_text)
		t_box.pack_start(show_hide_button,0,0,5)

		self.password_entry.grab_focus()

		t_box = Gtk.HBox()
		box.pack_start(t_box,0,0,5)
		root_user_button = Gtk.Button()
		root_user_button.set_label(_[127])
		root_user_button.connect("clicked",self.root_btn_click)
		t_box.pack_start(root_user_button,1,1,5)

		area = self.get_content_area()
		area.add(box)

		show_hide_button.grab_focus()

		self.connect("key-press-event",self.key_press)
		self.show_all()

	def password_text_change(self, widget):
		if self.first_insert:
			self.password_entry.set_visibility(False)
			self.first_insert = False
		

	def normal_btn_click(self,widget):
		self.response(Gtk.ResponseType.CANCEL)

	def root_btn_click(self,widget):
		self.response(Gtk.ResponseType.OK)

	def key_press(self,widget,event):
		key_name = Gdk.keyval_name(event.keyval)
		if key_name == "Return" or key_name == "KP_Enter":
			self.response(Gtk.ResponseType.OK)
		if self.first_insert:
			self.password_entry.grab_focus()

	def show_hide_text(self,widget):
		if self.password_entry.get_visibility():
			self.password_entry.set_visibility(False)
		else:
			self.password_entry.set_visibility(True)

if __name__ == '__main__':
	app = Application()
	app.run(sys.argv)
