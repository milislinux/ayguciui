import gi, os, subprocess
from gi.repository import Gtk, GLib
from agc_lib import md2
import configparser

class CommandAddEdit(Gtk.Popover):
	def __init__(self,parent,add_edit,_):
		super(CommandAddEdit,self).__init__()
		m_box = Gtk.VBox()
		self.add(m_box)
		self.parent = parent

		box = Gtk.HBox()
		cm_name_label = Gtk.Label()
		cm_name_label.set_text(_[103])
		box.pack_start(cm_name_label,0,0,5)
		self.cm_name = Gtk.Entry()
		box.pack_start(self.cm_name,1,1,5)
		m_box.pack_start(box,1,1,5)

		box = Gtk.HBox()
		cm_label = Gtk.Label()
		cm_label.set_text(_[104])
		box.pack_start(cm_label,0,0,5)
		self.cm = Gtk.Entry()
		box.pack_start(self.cm,1,1,5)
		m_box.pack_start(box,1,1,5)


		apply_button = Gtk.Button()
		m_box.pack_start(apply_button,1,1,5)
		if add_edit == "add":
			apply_button.set_label(_[105])
		else:
			self.cm_name.set_text(add_edit[0])
			self.cm.set_text(add_edit[1])
			apply_button.set_label(_[106])

			run_commmand = Gtk.Button()
			m_box.pack_start(run_commmand,1,1,5)
			run_commmand.set_label(_[113])
			run_commmand.connect("clicked",self.run_this_commmand)

			remove_button = Gtk.Button()
			m_box.pack_start(remove_button,1,1,5)
			remove_button.set_label(_[110])
			remove_button.connect("clicked",self.delete_user_dialog,add_edit,_)

		apply_button.connect("clicked",self.control_command,add_edit,_)

	def run_this_commmand(self,widget):
		subprocess.Popen(self.cm.get_text())

	def control_command(self,widget, add_edit,_):
		cm_name = self.cm_name.get_text()
		cm = self.cm.get_text()
		if cm_name == "":
			self.create_info_dialog(_[38],_[107])
		elif cm == "":
			self.create_info_dialog(_[38],_[108])
		else:
			keys_ = list(self.parent.autostarts.keys())
			if (add_edit == "add" and cm_name in keys_) or (add_edit != "add" and cm_name != add_edit[0] and cm_name in keys_):
				self.create_info_dialog(_[38],_[109])
			else:
				if add_edit != "add":
					self.parent.settings.remove_option("autostart",add_edit[0])
				self.parent.settings.set("autostart",cm_name,cm)
				with open(self.parent.config_file,"w") as config_obj:
					self.parent.settings.write(config_obj)
				self.parent.autostarts = self.parent.get_autostart()
				self.parent.add_autostarts()
				self.destroy()

	def delete_user_dialog(self,widget,add_edit,_):
		q = Gtk.MessageDialog(None,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,_[111])
		q.format_secondary_text(_[112].format(add_edit[0]))
		ans = q.run()
		if ans == Gtk.ResponseType.OK:
			self.parent.settings.remove_option("autostart",add_edit[0])
			with open(self.parent.config_file,"w") as config_obj:
				self.parent.settings.write(config_obj)
			self.parent.autostarts = self.parent.get_autostart()
			self.parent.add_autostarts()
			q.destroy()
			self.destroy()
		else:
			q.destroy()


	def create_info_dialog(self,title,text):
		dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, title)
		dialog.format_secondary_text(text)
		dialog.run()
		dialog.destroy()

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.parent = parent

		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[102]
		self.title = _[102]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("system-reboot",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		h_box = Gtk.HBox()
		main_box = Gtk.VBox()
		h_box.pack_start(main_box,True,True,10)
		self.add(h_box)
		
		self.command_add_button = Gtk.Button()
		self.command_add_button.connect("clicked",self.command_add,_)
		self.command_add_button.set_label(_[105])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_ADD,36)
		self.command_add_button.set_image(img)
		main_box.pack_start(self.command_add_button,0,1,5)

		self.autostarts = self.get_autostart()
		if self.autostarts:
			self.as_iw_store = Gtk.ListStore(str,str)
			self.as_iw = Gtk.TreeView(model=self.as_iw_store)
			self.as_iw.set_activate_on_single_click(True)
			self.as_iw.connect("row-activated",self.as_iw_activate,_)
			renderer = Gtk.CellRendererText()
			coloumn = Gtk.TreeViewColumn(_[103],renderer, text = 0)
			self.as_iw.append_column(coloumn)
			renderer = Gtk.CellRendererText()
			coloumn = Gtk.TreeViewColumn(_[104],renderer, text = 1)
			self.as_iw.append_column(coloumn)
			main_box.pack_start(self.set_scroll_win(self.as_iw),1,1,5)
			self.add_autostarts()

	def command_add(self, widget,_):
		cm_edit = CommandAddEdit(self,"add",_)
		cm_edit.set_position(Gtk.PositionType.BOTTOM)
		cm_edit.set_relative_to(widget)
		cm_edit.show_all()
		cm_edit.popup()

	def add_autostarts(self):
		self.as_iw_store.clear()
		as_keys = list(self.autostarts.keys())
		for as_ in as_keys:
			self.as_iw_store.append([as_,self.autostarts[as_]])


	def set_scroll_win(self,list_):
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(list_)
		return scroll

	def as_iw_activate(self,widget,path,coloumn,_):
		select = self.as_iw.get_selection()
		tree_model, tree_iter = select.get_selected()
		if tree_iter != None and tree_model != None:
			select = tree_model[tree_iter]
			cm_edit = CommandAddEdit(self,[select[0],select[1]],_)
			cm_edit.set_position(Gtk.PositionType.BOTTOM)
			cm_edit.set_relative_to(self.command_add_button)
			cm_edit.show_all()
			cm_edit.popup()

	def get_autostart(self):
		try:
			self.config_file = os.path.join(os.path.expanduser("~/.config/masa.ini"))
			if os.path.exists(self.config_file):
				self.settings = configparser.ConfigParser()
				self.settings.read(self.config_file)
				return self.settings["autostart"]
		except:
			pass
		return False
