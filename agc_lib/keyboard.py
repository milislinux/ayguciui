import gi, os, subprocess, cairo
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, GLib
from agc_lib import ayguci_parser, coordinats, keyboards

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[49]
		self.title = _[49]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("keyboard",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		m_box = Gtk.VBox()
		self.add(m_box)

		self.resim = Gtk.DrawingArea()
		self.resim.set_size_request (630, 200)
		self.resim.connect("draw", self.expose)
		m_box.pack_start(self.resim,0,0,5)

		box = Gtk.HBox()
		self.model_yazi = Gtk.Label()
		box.pack_start(self.model_yazi,0,0,5)
		self.model_combo = Gtk.ComboBoxText()
		box.pack_start(self.model_combo,1,1,5)
		m_box.pack_start(box,0,0,5)

		model = list(keyboards.modeller.keys())
		model.sort()


		self.is_wf_ini_get()

		sayac = 0
		for mod_ in model:
			self.model_combo.append_text(keyboards.modeller[mod_])
			if mod_ == self.key_model:
				self.model_combo.set_active(sayac)
			sayac += 1

		box = Gtk.HBox()
		self.ulke_yazi = Gtk.Label()
		box.pack_start(self.ulke_yazi,0,0,5)
		self.ulke_combo = Gtk.ComboBoxText()
		box.pack_start(self.ulke_combo,1,1,5)

		sayac = 0
		for duzen in keyboards.duzenler.keys():
			self.ulke_combo.append_text(keyboards.duzenler[duzen])
			if duzen == self.key_layout:
				self.ulke_combo.set_active(sayac)
			sayac += 1

		self.tur_yazi = Gtk.Label()
		box.pack_start(self.tur_yazi,0,0,5)
		self.tur_combo = Gtk.ComboBoxText()
		box.pack_start(self.tur_combo,1,1,5)
		m_box.pack_start(box,0,0,5)

		sayac = 0
		self.tur_combo.append_text("Default")
		self.tur_combo.set_active(sayac)
		for k in keyboards.varyantlar[self.key_layout]:
			variant = list(k.values())[0]
			self.tur_combo.append_text(variant)
			if variant == self.key_variant:
				self.tur_combo.set_active(sayac)
			sayac += 1


		self.text_entry = Gtk.Entry()
		m_box.pack_start(self.text_entry,0,0,5)

		self.model_yazi.set_text(_[50])
		self.ulke_yazi.set_text(_[51])
		self.tur_yazi.set_text(_[52])

		self.model_combo.connect("changed", self.klavye_modeli_secildi)
		self.ulke_combo.connect("changed", self.ulke_secildi)
		self.tur_combo.connect("changed", self.tur_secildi)

		self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)


	def is_wf_ini_get(self):
		masa_dir = os.path.join(os.path.expanduser("~/.config/masa.ini"))
		if os.path.exists(masa_dir):
			self.settings = GLib.KeyFile()
			self.settings.load_from_file(masa_dir, GLib.KeyFileFlags.NONE)
			self.key_layout = self.settings.get_string("keyboard","layout")
			self.key_model = self.settings.get_string("keyboard","model")
			if self.key_model == "":
				self.key_model = "pc105"
			self.key_variant = self.settings.get_string("keyboard","variant")
			return True
		#wf_shell_dir = os.path.join(os.path.expanduser("~/.config/wayfire.ini"))
		#if os.path.exists(wf_shell_dir):
		#	self.settings = GLib.KeyFile()
		#	self.settings.load_from_file(wf_shell_dir, GLib.KeyFileFlags.NONE)
		#	self.key_layout = self.settings.get_string("input","xkb_layout")
		#	self.key_model = self.settings.get_string("input","xkb_model")
		#	if self.key_model == "":
		#		self.key_model = "pc105"
		#	self.key_variant = self.settings.get_string("input","xkb_variant")
		#	return True
		return False


	def klavye_modeli_secildi(self,widget):
		model = list(keyboards.modeller.keys())
		for mod_ in model:
			if keyboards.modeller[mod_] == self.model_combo.get_active_text():
				self.key_model = mod_
				self.settings.set_string("keyboard","model",self.key_model)
				self.settings.save_to_file(os.path.expanduser("~/.config/masa.ini"))
		self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)


	def ulke_secildi(self,widget):
		for duzen in keyboards.duzenler.keys():
			if keyboards.duzenler[duzen] == self.ulke_combo.get_active_text():
				self.key_layout = duzen
				self.settings.set_string("keyboard","layout",self.key_layout)
				self.settings.save_to_file(os.path.expanduser("~/.config/masa.ini"))

		self.tur_combo.remove_all()
		self.key_variant = ""
		self.tur_combo.append_text("Default")
		try:
			print(self.key_variant)
			for k in keyboards.varyantlar[self.key_layout]:
				self.tur_combo.append_text(list(k.values())[0])
		except:
			pass
		self.tur_combo.set_active(0)

	def tur_secildi(self,widget):
		if self.tur_combo.get_active_text() == "Default":
			self.key_variant = ""
			self.settings.set_string("keyboard","variant",self.key_variant)
			self.settings.save_to_file(os.path.expanduser("~/.config/masa.ini"))
			self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)
		else:
			try:
				for k in keyboards.varyantlar[self.key_layout]:
					if list(k.values())[0] == self.tur_combo.get_active_text():
						self.key_variant = list(k.keys())[0]
						self.settings.set_string("keyboard","variant",self.key_variant)
						self.settings.save_to_file(os.path.expanduser("~/.config/masa.ini"))
						self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)
			except:
				pass


	def duzenler_doldur(self):
		self.ulke_combo.remove_all()
		keys = list(keyboards.duzenler.keys())
		keys.sort()
		for key_ in keys:
			self.ulke_combo.append_text(keyboards.duzenler[key_])
		self.ulke_combo.set_active(0)

	def expose(self, widget, cr):
		klavye = cairo.ImageSurface.create_from_png("./icons/klavye.png")
		cr.set_source_surface(klavye,0,0)
		cr.paint()
		koordinat_listesi = ((10, 20), (65, 65), (85, 105), (60, 150))
		sayac = 0
		if self.key_model in keyboards.klavye_modeli.keys():
			for key_list in keyboards.klavye_modeli[self.key_model]:
				coordinat = koordinat_listesi[sayac]
				sayac += 1
				for num, key in enumerate(key_list):
					try:
						cr.set_source_rgb(204, 255, 0)
						cr.select_font_face("Noto Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
						cr.set_font_size(10)
						cr.move_to(coordinat[0]+(42*num), coordinat[1])
						cr.show_text(self.tus_yerlesimi[key][1])
						cr.set_source_rgb(255, 255, 255)
						cr.set_font_size(12)
						cr.move_to(coordinat[0]+10+(42*num), coordinat[1]+15)
						cr.show_text(self.tus_yerlesimi[key][0])
					except KeyError as err:
						print(key)

	def cizim_baslangic(self, layout, model, variant):
		if model in keyboards.klavye_modeli.keys():
			self.tus_yerlesimi = self.unicodeToString(model, layout, variant)
			if self.tus_yerlesimi:
				self.resim.queue_draw()

	def unicodeToString(self, model, layout, variant=""):
		try:
			keycodes = {}
			tus_yerlesimi_command = subprocess.Popen(["./sh/ckbcomp", "-model", model, "-layout", layout, "-variant", variant],
												stdout=subprocess.PIPE)
			ciktilar = tus_yerlesimi_command.stdout.read()
			for cikti in ciktilar.decode("utf-8").split("\n"):
				if cikti.startswith("keycode") and cikti.count("="):
					cikti = cikti.split()
					if cikti[3].startswith("U+") or cikti[3].startswith("+U"):
						first = bytes("\\u" + cikti[3][2:].replace("+", ""), "ascii").decode("unicode-escape")
						second = bytes("\\u" + cikti[4][2:].replace("+", ""), "ascii").decode("unicode-escape")
						keycodes[int(cikti[1])] = [first, second]
			return keycodes
		except:
			return False

