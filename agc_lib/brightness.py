import gi, os, subprocess, json
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, Gio, GLib

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_,backlights):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[129]
		self.title = _[129]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("brightnesssettings",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		screens_label = Gtk.Label()
		screens_label.set_text(_[128])
		c_box.pack_start(screens_label,False,False,5)
		self.screens_light_combo = Gtk.ComboBoxText()
		
		for light_name in backlights:
			self.screens_light_combo.append_text("/sys/class/backlight/"+str(light_name))

		c_box.pack_start(self.screens_light_combo,True,True,5)
		self.screens_light_combo.set_active(0)
		self.apply_light_button = Gtk.Button()
		self.apply_light_button.set_label(_[20])
		self.apply_light_button.connect("clicked",self.change_light_settings)
		c_box.pack_start(self.apply_light_button,True,True,5)
		
	def change_light_settings(self,widget):
		widget.set_sensitive(False)
		light_name = self.screens_light_combo.get_active_text()
		light_name = light_name.split("/")[-1]
		wf_settings = os.path.expanduser("~/.config/wayfire.ini")
		g_settings = GLib.KeyFile()
		if os.path.exists(wf_settings):
			g_settings.load_from_file(wf_settings,GLib.KeyFileFlags.NONE)
		else:
			g_settings = False

		if g_settings:
			down = "light -s {} -U 5 && light -O".format("sysfs/backlight/"+light_name)
			up = "light -s {} -A 5 && light -O".format("sysfs/backlight/"+light_name)
			g_settings.set_string("command","command_light_down",down)
			g_settings.set_string("command","command_light_up",up)
			g_settings.save_to_file(wf_settings)

		waybar_dict = self.parent.waybar_parse_hjson()
		if waybar_dict:
			waybar_dict["backlight"]["on-scroll-up"] = "light -s {} -U 5 && light -O".format("sysfs/backlight/"+light_name)
			waybar_dict["backlight"]["on-scroll-down"] = "light -s {} -A 5 && light -O".format("sysfs/backlight/"+light_name)
			
			waybar_dict["backlight"]["device"] = light_name
			
			with open(os.path.expanduser("~/.config/waybar/config"),"w") as f:
				json.dump(waybar_dict, f, indent=4, ensure_ascii=False)

			subprocess.Popen(["killall","waybar"], stdout = subprocess.PIPE)
			GLib.timeout_add(1000,self.run_waybar)
			

	def run_waybar(self):
		
		subprocess.Popen(["waybar"], stdout = subprocess.PIPE)
		self.apply_light_button.set_sensitive(True)
		return False
