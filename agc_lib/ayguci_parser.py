import subprocess, json, os, gi
from gi.repository import Gtk


win = False
user_passwd = None

def create_debug_window():
	global win
	global debugview
	win = Gtk.Window()
	scrolledwindow = Gtk.ScrolledWindow()
	scrolledwindow.set_hexpand(True)
	scrolledwindow.set_vexpand(True)
	win.add(scrolledwindow)
	debugview = Gtk.TextView()
	scrolledwindow.add(debugview)
	win.set_size_request(640,480)
	win.show_all()

def run_process(arg):
	global user_passwd
	s_dir = "/usr/milis/ayguciui/ayguci/cli.lua"
	get = subprocess.Popen(["sh","-c","echo {}|sudo -S {} '{}'".format(user_passwd,s_dir,arg)], stdout = subprocess.PIPE)
	get.wait()
	get = get.communicate()[0]
	get = get.decode("utf-8","strict")
	if win:
		db_buffer = debugview.get_buffer()
		end_iter = db_buffer.get_end_iter()
		db_buffer.insert(end_iter,"Komut >>>> {}\n".format(arg))
		end_iter = db_buffer.get_end_iter()
		db_buffer.insert(end_iter,"Dönüt <<<< {}\n".format(json.loads(get)))
	return get

def delete_user(user_name,force):
	r_t = run_process('account/user^{"method":"post","oper":"del","user":"'+user_name+'","force":'+force+'}')

def get_all_user_groups():
	r_t = run_process('account/group^{"method":"get","query":"all"}')
	return json.loads(r_t)

def get_user_groups(user_name):
	r_t = run_process('account/group^{"method":"get","user":"'+user_name+'","query":"info"}')
	return json.loads(r_t)

def get_default_user_groups():
	r_t = run_process('account/group^{"method":"get","query":"default"}')
	return json.loads(r_t)

def get_all_users():
	r_t = run_process('account/user^{"method":"get"}')
	return json.loads(r_t)

def get_normal_users():
	r_t = run_process('account/user^{"method":"get","query":"normal"}')
	return json.loads(r_t)

def change_user_pass(user_name,new_pass):
	new_pass = pass_to_hash(new_pass)
	r_t = run_process('account/user^{"method":"post","oper":"chpass","user":"'+user_name+'","newpasswd":"'+new_pass+'"}')
	return json.loads(r_t)

def pass_to_hash(passwd,salt=False):
	get = subprocess.Popen(["sh","-c",'echo "{}" | openssl passwd -6 -stdin'.format(passwd)], stdout = subprocess.PIPE)
	get.wait()
	get = get.communicate()[0]
	get = get.decode("utf-8","strict")
	return get[:-1]

def add_user(user_name,desc,home,groups):
	r_t = run_process('account/user^{"method":"post","oper":"add","user":"'+user_name+'","desc":"'+desc+'","home":"'+home+'","groups":["'+groups+'"]}')
	return json.loads(r_t)

def get_timezone():
	r_t = run_process('datetime/timezone^{"method":"get"}')
	return json.loads(r_t)

def set_timezone(zone):
	r_t = run_process('datetime/timezone^{"method":"post","zone":"'+zone+'"}')

def set_hostname(h_name):
	r_t = run_process('system/info^{"method":"post","hostname":"'+h_name+'"}')
	return json.loads(r_t)

def get_system_info():
	r_t = run_process('system/info^{"method":"get"}')
	return json.loads(r_t)

def get_datetime_utc():
	r_t = run_process('datetime/utc^{"method":"get"}')
	return json.loads(r_t)

def set_datetime_utc(status):
	r_t = run_process('datetime/utc^{"method":"post","utc_status":"'+status+'"}')

def set_datetime_manuel(time):
	r_t = run_process('datetime/manual^{"method":"post","value":"'+time+'"}')

def test_ayguci():
	if os.path.exists("/usr/milis/ayguciui/ayguci/cli.lua"):
		r_t = run_process('datetime/timezone^{"method":"get"}')
		try:
			json.loads(r_t)
			return True
		except:
			return False
	else:
		return False
