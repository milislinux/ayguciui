import gi, os, subprocess
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from agc_lib import ayguci_parser

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[3]
		self.title = _[3]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("dialog-information",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		self.system_icon = Gtk.Image.new_from_file("./icons/milis-icon.svg")
		main_box.pack_start(self.system_icon,0,1,5)

		self.os_linux_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_linux_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_version_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_version_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_release_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_release_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_kernel_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_kernel_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.product_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.product_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.serial_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.serial_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.uuid_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.uuid_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.board_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.board_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_hostname_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_hostname_label,0,0,5)

		self.edit_host_button = Gtk.Button()
		self.edit_host_button.connect("clicked",self.edit_host)
		img = Gtk.Image.new_from_stock(Gtk.STOCK_EXECUTE,36)
		self.edit_host_button.set_image(img)
		box.pack_start(self.edit_host_button,0,0,5)

		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.ram_icon = Gtk.Image.new_from_file("./icons/ram.svg")
		main_box.pack_start(self.ram_icon,0,1,5)

		self.ram_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.ram_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.cpu_icon = Gtk.Image.new_from_file("./icons/cpu.svg")
		main_box.pack_start(self.cpu_icon,0,1,5)

		self.cpu_model_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.cpu_model_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.cpu_core_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.cpu_core_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.s_card_icon = Gtk.Image.new_from_file("./icons/s_card.svg")
		main_box.pack_start(self.s_card_icon,0,1,5)

		self.video_vga_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.video_vga_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.video_3d_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.video_3d_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.bios_icon = Gtk.Image.new_from_file("./icons/bios.svg")
		main_box.pack_start(self.bios_icon,0,1,5)

		self.bios_vendor_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_vendor_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.bios_version_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_version_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.bios_date_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_date_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.update_ui()

	def edit_host(self,widget):
		dw = Gtk.MessageDialog(None,
						Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
						Gtk.MessageType.QUESTION,
						Gtk.ButtonsType.OK_CANCEL,
						self._[47])

		dw.set_title(self._[100])

		dialogBox = dw.get_content_area()
		userEntry = Gtk.Entry()
		#userEntry.set_visibility(False)
		userEntry.set_size_request(250,0)
		dialogBox.pack_end(userEntry, False, False, 0)

		dw.show_all()
		response = dw.run()
		text = userEntry.get_text() 
		dw.destroy()
		if (response == Gtk.ResponseType.OK) and (text != ''):
			ayguci_parser.set_hostname(text)
			self.update_ui()

	def update_ui(self):
		info = ayguci_parser.get_system_info()["data"]
		try:
			#self.os_linux_label.set_text(self._[22]+info['os']['linux'])
			self.os_version_label.set_text(self._[23]+info['os']['version'])
			self.os_release_label.set_text(self._[24]+info['os']['release'])
			self.os_kernel_label.set_text(self._[25]+info['os']['kernel'])
			self.product_label.set_text(self._[4]+info['vendor']+" "+info['product'])
			self.serial_label.set_text(self._[5]+info['serial'])
			self.uuid_label.set_text(self._[6]+info['uuid'])
			self.board_label.set_text(self._[7]+info['board'])
			self.os_hostname_label.set_text(self._[8]+info['os']['hostname'])
			self.ram_label.set_text(self._[9]+info['ram'].replace("MemTotal:",""))
			self.cpu_model_label.set_text(self._[10]+info['cpu']['model'])
			self.cpu_core_label.set_text(self._[11]+info['cpu']['core'])
			self.video_vga_label.set_text(self._[12]+info['video']['VGA'])
			self.video_3d_label.set_text(self._[13]+info['video']['3D'])
			self.bios_vendor_label.set_text(self._[14]+info['bios']['vendor'])
			self.bios_version_label.set_text(self._[15]+info['bios']['version'])
			self.bios_date_label.set_text(self._[16]+info['bios']['date'])
		except:
			pass
