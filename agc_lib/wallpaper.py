import gi, os, subprocess, threading
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib


class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = _[53]
		self.title = _[53]
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("ristretto",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,True,5)
		self.images_store = Gtk.ListStore(str,GdkPixbuf.Pixbuf)
		self.images_list = Gtk.IconView(model=self.images_store)
		self.images_list.set_text_column(0)
		self.images_list.set_pixbuf_column(1)
		self.images_list.set_item_width(120)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.images_list)
		c_box.pack_start(scroll,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.file_choser = Gtk.FileChooserButton()
		self.file_choser.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
		self.file_choser.connect("file-set",self.select_folder)
		c_box.pack_start(self.file_choser,False,False,5)

		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.apply_button_clicked)
		c_box.pack_start(self.apply_button,True,True,5)

		self.set_labels(_)
		self.is_wf_shell_get()

	def is_wf_shell_get(self):
		wf_shell_dir = os.path.join(os.path.expanduser("~/.config/masa.ini"))
		if os.path.exists(wf_shell_dir):
			self.settings = GLib.KeyFile()
			self.settings.load_from_file(wf_shell_dir, GLib.KeyFileFlags.NONE)
			try:
				background = self.settings.get_string("autostart","background")
				background = background.split()
				background = background[2]
				dirs = os.path.split(background)
				self.file_choser.set_current_folder(dirs[0])
				thread_ = threading.Thread(target=self.add_images, args=[dirs[0]])
				thread_.start()
			except:
				pass
					
	def apply_button_clicked(self,widget):
		selection = self.images_list.get_selected_items()
		if len(selection) == 0:
			return False
		else:
			selection = selection[0]
		image = self.images_store[selection][0]
		folder = self.file_choser.get_filename()
		dir_ = os.path.join(folder,image)

		command = "swaybg -i {}".format(dir_)
		try:
			run = subprocess.Popen(["killall","swaybg"])
			run.wait()
			subprocess.Popen(command.split())
		except:
			pass
		self.settings.set_string("autostart","background",command)
		self.settings.save_to_file(os.path.expanduser("~/.config/masa.ini"))

	def add_images(self,folder):
		self.images_store.clear()
		images = os.listdir(folder)
		images.sort()
		for file_ in images:
			if folder != self.file_choser.get_current_folder():
				break
			name, type_ = os.path.splitext(file_)
			if type_ in [".png",".jpg",".jpeg"]:
				p_b = GdkPixbuf.Pixbuf.new_from_file_at_size(folder+"/"+file_,
															160,
															90)
				self.images_store.append([file_,p_b])


	def set_labels(self,_):
		self.apply_button.set_label(_[20])
		#self.randomize_check_b.set_label(_[54])
		#self.preserve_aspect_check_b.set_label(_[55])
		#self.type_label.set_text(_[56])


	def select_folder(self,widget):
		folder = self.file_choser.get_filename()
		thread_ = threading.Thread(target=self.add_images, args=[folder])
		thread_.start()
